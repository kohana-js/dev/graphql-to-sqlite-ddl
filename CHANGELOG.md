# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [2.0.4](https://gitlab.com/kojin-nakana/graphql-to-sqlite-ddl/compare/v2.0.3...v2.0.4) (2022-04-28)


### Bug Fixes

* primary key error ([4f5946d](https://gitlab.com/kojin-nakana/graphql-to-sqlite-ddl/commit/4f5946d009dd733d3b6aa031d22a328d56dce757))

### [2.0.3](https://gitlab.com/kojin-nakana/graphql-to-sqlite-ddl/compare/v2.0.2...v2.0.3) (2022-04-28)


### Bug Fixes

* auto increment SQLite ([a165498](https://gitlab.com/kojin-nakana/graphql-to-sqlite-ddl/commit/a1654984eae1b0bac5cf1e4af5712d8ac9a95661))

### [2.0.2](https://gitlab.com/kojin-nakana/graphql-to-sqlite-ddl/compare/v2.0.1...v2.0.2) (2022-02-17)


### Bug Fixes

* insert data with null ([da89bb1](https://gitlab.com/kojin-nakana/graphql-to-sqlite-ddl/commit/da89bb1c469e45720ceb966d54e7389113e41787))

### 2.0.1 (2022-01-13)


### Bug Fixes

* joint table key pair is unique. pluralize table name ([9496db2](https://gitlab.com/kojin-nakana/graphql-to-sqlite-ddl/commit/9496db21451fbd96ef1c28631206c96524dce680))
